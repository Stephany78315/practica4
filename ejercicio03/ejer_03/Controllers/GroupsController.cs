﻿using UPB.Practice4.Logic.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using UPB.Practice4.Logic.Managers;
using UPB.Practice4.Logic;

namespace ejer01.Controllers
{
    [ApiController]
    [Route("/api/groups")]
    public class GroupsController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IGroupManager _groupManager;
        public GroupsController(IConfiguration config, IGroupManager groupManager)
        {
            _config = config;
            _groupManager = groupManager;
        }

        [HttpGet]
        public List<Group> GetGroups()
        {
            return _groupManager.GetAllGroups();
        }

        


        [HttpPost]
        public Group CrateStudent([FromBody] Group person)
        {
           return _groupManager.CreateGroup(person);
        }

        [HttpPut]
        public Group UpateStudent([FromBody] Group student)
        {
           return  _groupManager.UpdateGroup(student);
        }

        [HttpDelete]
        public Group DeleteStudent([FromBody] Group student)
        {
            return _groupManager.DeleteGroup(student);
        }

    }
}




