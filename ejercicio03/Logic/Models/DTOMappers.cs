﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UPB.Practice4.Logic.Models
{
    public static class DTOMappers
    {
        public static List<Group> MapGroups(List<Data.Models.Group> group)
        {
            List<Group> mappedStudents = new List<Group>();

            foreach(Data.Models.Group s in group)
            {
                mappedStudents.Add(new Group()
                {
                    Id = s.Id,
                    Name = s.Name,
                    AvailableSlots = s.AvailableSlots

                }); 
            }
            return mappedStudents;
        }


        public static Group MapGroupDL(Practice4.Data.Models.Group group)
        {


            Group nuevo = new Group()
            {
                Id = group.Id,
                Name = group.Name,
                AvailableSlots = group.AvailableSlots
            };
                
            
            return nuevo;
        }
        public static Practice4.Data.Models.Group MapGroupLD(Group group)
        {


            Practice4.Data.Models.Group nuevo = new Practice4.Data.Models.Group()
            {
                Id = group.Id,
                Name = group.Name,
                AvailableSlots = group.AvailableSlots
            };


            return nuevo;
        }


    }
}
