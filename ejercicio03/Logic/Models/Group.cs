﻿namespace UPB.Practice4.Logic.Models
{
    //DTO = Data Transfer Object
    public class Group
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public int AvailableSlots { get; set; }

    }
}
