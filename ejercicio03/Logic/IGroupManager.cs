﻿using System;
using System.Collections.Generic;
using System.Text;
using UPB.Practice4.Logic.Models;

namespace UPB.Practice4.Logic
{
    public interface IGroupManager
    {
        public List<Group> GetAllGroups();
        public Group CreateGroup(Group group);
        public Group UpdateGroup(Group group);
        public Group DeleteGroup(Group group);
    }
}
