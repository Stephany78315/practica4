﻿using UPB.Practice4.Logic.Models;
using System;
using System.Collections.Generic;
using System.Text;
using UPB.Practice4.Data;

namespace UPB.Practice4.Logic.Managers
{
    public class GroupManager: IGroupManager
    {
        private readonly IDbContext _dbContext;

        public GroupManager(IDbContext dbContext)
        {
            _dbContext = dbContext;

        }
        public List<Group> GetAllGroups()
        {
            /*string projectTitle = _config.GetSection("Project").GetSection("Title").Value;
            string dbConnection = _config.GetConnectionString("Database");
            Console.Out.WriteLine($"We are in env :{projectTitle}");
            Console.Out.WriteLine($"We are connecting to: {dbConnection}");
            */

            // return _dbLayer.GetStudent();
            /*
            return new List<Student>()
            {
                // new Student() {Name = $"Pablo from env: {projectTitle}, " },
                new Student() {Name = "Pablo" },
                new Student() {Name = "Monica" },
                new Student() {Name = "Juan" }
            };*/
            List<Data.Models.Group> group = _dbContext.GetAll();
            return DTOMappers.MapGroups(group);

        }

        public Group CreateGroup(Group group)
        {
            List<Data.Models.Group> group2 = _dbContext.GetAll();

            string compa = $"Group-00{group2.Count}";
            if(String.IsNullOrEmpty(group.Name) && group.Name.Length < 50)
            {
                throw new Exception();
            }
            
            group.Id = compa;

            if(group.AvailableSlots < 0)
            {
                throw new Exception();
            }
            
            _dbContext.AddGroup(DTOMappers.MapGroupLD(group));
            return group;
            
           
        }
        public Group UpdateGroup(Group group)
        {
            _dbContext.UpdateGroup(DTOMappers.MapGroupLD(group));
            return group;
        }
        public Group DeleteGroup(Group group)
        {
            _dbContext.DeleteGroup(DTOMappers.MapGroupLD(group));
            return group;
        }











    }
}
