﻿using System;

namespace UPB.Practice4.Data.Models
{
    // DAO = Data Access Object
    public class Group
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int AvailableSlots { get; set; }
    }
}
