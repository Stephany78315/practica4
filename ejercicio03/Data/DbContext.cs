﻿using System;
using System.Collections.Generic;
using System.Text;
using UPB.Practice4.Data.Models;

namespace UPB.Practice4.Data
{
    public class DbContext : IDbContext
    {
        // Access to DB server
        // References to DB tables (entities)

        public List<Group> StudentTable { get; set; }
        public DbContext()
        {
            //Read from config file the DB Connection String
            // ORM for Net Core is (Entity Framework Core)
            // Review Repository Pattern
            StudentTable = new List<Group>()
            {
                // new Student() {Name = $"Pablo from env: {projectTitle}, " },
                new Group() { Id = "Group-000", Name = "Pablo" , AvailableSlots = 2},
                new Group() { Id = "Group-001", Name = "Monica", AvailableSlots = 3},
                new Group() { Id = "Group-002", Name = "Juan", AvailableSlots = 5}
            
            };
        }

        public Group AddGroup(Group student)
        {
            StudentTable.Add(student);
            return student;
        }
        public Group  UpdateGroup(Group studentToUpdate)
        {
            //Select * from Student where name = "Student Name"
            Group foundStudent = StudentTable.Find(student => student.Id == studentToUpdate.Id);
            foundStudent.Name = studentToUpdate.Name;
            foundStudent.AvailableSlots = studentToUpdate.AvailableSlots;

            return foundStudent;
        }
        public Group DeleteGroup(Group studentToDelete)
        {
            StudentTable.RemoveAll(student => student.Id == studentToDelete.Id);
          
            return studentToDelete;
        }

        public List<Group> GetAll()
        {
            return StudentTable;
        }



        ///Temrinar para la practica

    }
}
