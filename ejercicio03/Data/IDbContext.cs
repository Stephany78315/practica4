﻿using System;
using System.Collections.Generic;
using System.Text;
using UPB.Practice4.Data.Models;

namespace UPB.Practice4.Data
{
    public interface IDbContext
    {
        Group AddGroup(Group student);
        public Group UpdateGroup(Group studentToUpdate);
        public Group DeleteGroup(Group studentToDelete);
        public List<Group> GetAll();
        
    }
}
